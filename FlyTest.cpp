#include <iostream>
#include <cgeomathbase/CommonGeoMath.h>

#include <DJIFlyController/DJIFlyController.h>

using namespace std;
using namespace CGM;
using namespace FlyingPlatform;

int main(int argc, char** argv)
{
	DJIFlyController controller;
	Recv recv;

	QObject::connect(&controller, &DJIFlyController::telemetryWasUpdated, &recv, &Recv::onDataReceived,Qt::ConnectionType::DirectConnection);

	while (true)
	{
		usleep(5e5);
	}

	return 1;
}