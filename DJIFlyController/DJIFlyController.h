#ifndef DJIFLYCONTROLLER_H
#define DJIFLYCONTROLLER_H

#include <string>
#include <iostream>
#include <chrono>

#include <dji_status.hpp> 
#include <dji_vehicle.hpp>

#include <qobject.h>
#include <qmetatype.h>

#include "../cgeomathbase/CommonGeoMath.h"

namespace FlyingPlatform
{
	enum LandingGearState
	{
		UNDIFINED = 0,
		DOWN = 1,
		UP = 2,
		IN_MOVE = 3
	};

	enum FlightState
	{
		STOPED = 0,
		ON_GROUND = 1,
		IN_AIR = 2
	};

	struct Q_HwTelemetry
	{
		qreal gpsPosition_lat=0;
		qreal gpsPosition_lng=0;
		qreal gpsPosition_alt=0;

		qreal qx=0;
		qreal qy=0;
		qreal qz=0;
		qreal qw=0;

		qreal velocityX=0; //m/s
		qreal velocityY=0; //m/s
		qreal velocityZ=0; //m/s

		quint8 gpsHealth=0; // from 0 to 5

		qint16 gpsSattelitesCount=0;
		
		qint16 voltage=0;

		bool controlledBySDK=false;
		
		qint16 flightState=0;

		qint16 landingGearState=0;
		
		qint64 timestamp=0;
	};

	struct HwTelemetry
	{
		HwTelemetry(Q_HwTelemetry q) :
			gpsPosition(q.gpsPosition_lat, q.gpsPosition_lng, q.gpsPosition_alt),
			orientation(q.qw,q.qx,q.qy,q.qz),
			absVelocity(q.velocityX,q.velocityY,q.velocityZ),
			gpsSattelitesCount(q.gpsSattelitesCount),
			voltage(q.voltage),
			controlledBySDK(q.controlledBySDK),
			flightState(static_cast<FlightState>(q.flightState)),
			landingGearState(static_cast<LandingGearState>(q.landingGearState)),
			timestamp(q.timestamp)
		{

		}

		CGM::v3geo gpsPosition;
		CGM::quat orientation;
		CGM::v3 absVelocity;

		quint8 gpsHealth;
		int gpsSattelitesCount;
		
		int voltage;
		bool controlledBySDK;
		
		FlightState flightState;
		LandingGearState landingGearState;
		
		std::chrono::milliseconds timestamp;
	};

	static const DJI::OSDK::Telemetry::TopicName stdTopicList[] = {
	DJI::OSDK::Telemetry::TOPIC_RC_FULL_RAW_DATA,
	DJI::OSDK::Telemetry::TOPIC_BATTERY_INFO,
	DJI::OSDK::Telemetry::TOPIC_VELOCITY,
	DJI::OSDK::Telemetry::TOPIC_QUATERNION,
	DJI::OSDK::Telemetry::TOPIC_CONTROL_DEVICE,
	DJI::OSDK::Telemetry::TOPIC_STATUS_FLIGHT
	};
	static const int stdTopicList_size = sizeof(stdTopicList) / sizeof(stdTopicList[0]);

		class DJIFlyController :public QObject
	{
		Q_OBJECT
	public:
		DJIFlyController(const char* device = "/dev/ttyUSB0", uint32_t baudRate = 230400);
		~DJIFlyController();

	signals:
		void telemetryWasUpdated(Q_HwTelemetry telem);

	private:
		Vehicle* v;
		int subscribePkgCount;
		void subscribeOnBasicTopics();
		void updPrivateTelemetry(DJI::OSDK::Telemetry::TopicName topic);

		Q_HwTelemetry telemetry;
		bool authoritySwitchOn; //���� ������� �� ������ � ������ ������ - true, ����� - false
		bool authority; //���� ���� ����������� ����� SDK - true, ����� - false

		static void activateCallback(Vehicle* v, RecvContainer recvFrame, UserData userData);
		static void stdPkgReceiveCallback(Vehicle* v, RecvContainer recvFrame, UserData userData);

	};

		class Recv: public QObject
		{
			Q_OBJECT
		public:
			Recv()
			{
			}

			~Recv()
			{
			}
		public slots:
			void onDataReceived(const Q_HwTelemetry& telem)
			{
				HwTelemetry t(telem);

				std::cout << t.gpsPosition.toStr(5) << std::endl;
				std::cout << int(t.gpsHealth) << std::endl;
				std::cout << t.gpsSattelitesCount << std::endl;
			}

		private:

		};

}

Q_DECLARE_METATYPE(FlyingPlatform::Q_HwTelemetry)
static int qMetaType_HwTelemetry_id = qRegisterMetaType<FlyingPlatform::Q_HwTelemetry>("FlyingPlatform::Q_HwTelemetry");

#endif // !DJIFLYCONTROLLER_H

