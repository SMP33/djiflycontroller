#include "DJIFlyController.h"

using namespace std;
using namespace CGM;
using namespace DJI::OSDK::Telemetry;
using namespace FlyingPlatform;

DJIFlyController::DJIFlyController(const char* device, uint32_t baudRate) :
	subscribePkgCount(0),
	authoritySwitchOn(false),
	authority(false),
	telemetry(Q_HwTelemetry()),
	v(new Vehicle(device, baudRate, true))
{
	if(!v->protocolLayer->getDriver()->getDeviceStatus())
		cout << "Comms appear to be incorrectly set up. Exiting." << endl;

	Vehicle::ActivateData* activateData=new Vehicle::ActivateData();

	activateData->encKey= "605403f6756f9a0f502837ff0ab2c441200ed3f8bccdcd22d32d18f2f1552005";
	activateData->ID = 1086388;
	activateData->version = v->getFwVersion();

	v->activate(activateData,activateCallback,this);
	
}

DJIFlyController::~DJIFlyController()
{
}

void DJIFlyController::activateCallback(Vehicle* v, RecvContainer recvFrame, UserData userData)
{
	DJIFlyController* c = static_cast<DJIFlyController*>(userData);
	c->subscribeOnBasicTopics();
	
}

void DJIFlyController::subscribeOnBasicTopics()
{
	int pkgID = 0;

	DJI::OSDK::Telemetry::TopicName* tmpTopicList = new DJI::OSDK::Telemetry::TopicName[stdTopicList_size];

	for (int i = 0; i < stdTopicList_size; i++)
	{
		tmpTopicList[i] = stdTopicList[i];
	}

	v->subscribe->verify(5000);

	if (v->subscribe->initPackageFromTopicList(pkgID, stdTopicList_size, tmpTopicList, false, 50))
	{
		v->subscribe->startPackage(pkgID);
		usleep(1000000);

		v->subscribe->registerUserPackageUnpackCallback(pkgID, stdPkgReceiveCallback, this);
	}
	else
	{
		cout << "EXIT" << endl;
		exit(-1);
	}
}

void DJIFlyController::stdPkgReceiveCallback(Vehicle* v, RecvContainer recvFrame, UserData userData)
{
	DJIFlyController* c = static_cast<DJIFlyController*>(userData);

	for (int i = 0; i < stdTopicList_size; i++)
	{
		c->updPrivateTelemetry(stdTopicList[i]);
	}

	auto g = v->broadcast->getGlobalPosition();

	Q_HwTelemetry t;
	t.gpsPosition_alt = g.altitude;
	t.gpsPosition_lat = g.latitude*RAD2DEG;


	emit c->telemetryWasUpdated(t);
}

void DJIFlyController::updPrivateTelemetry(DJI::OSDK::Telemetry::TopicName topic)
{
	switch (topic)
	{
	case TopicName::TOPIC_BATTERY_INFO:
	{
		telemetry.voltage = v->subscribe->getValue<TOPIC_BATTERY_INFO>().voltage;
	}
		break;

	case TopicName::TOPIC_RC_FULL_RAW_DATA:
	{
		auto data_RC = 
			v->subscribe->getValue<DJI::OSDK::Telemetry::TOPIC_RC_FULL_RAW_DATA>();

		//��� ���������� ������ ��������� ��������� ����� � Assisntant'� ���, ����� � ������� F ��� ����� P
		//��������� ��������� ����� ������
		//https://developer.dji.com/onboard-api-reference/structDJI_1_1OSDK_1_1Telemetry_1_1LB2RcFullRawData.html#a460f0063ce23e3d6471e2cdea744573a

		authoritySwitchOn = (data_RC.lb2.mode == 1684) ? true: false;
	}
		break;

	case TopicName::TOPIC_CONTROL_DEVICE:
	{
		auto data_ctrlDevice =
			v->subscribe->getValue<DJI::OSDK::Telemetry::TOPIC_CONTROL_DEVICE>();

		//������ ����� ������
		//https://developer.dji.com/onboard-api-reference/structDJI_1_1OSDK_1_1Telemetry_1_1SDKInfo.html#a493c31f8ea34f5bd17808084931b2262

		authority = (data_ctrlDevice.deviceStatus==2)? true: false;
	}
		break;
		
	case TopicName::TOPIC_VELOCITY:
	{
		auto velocity =
			v->subscribe->getValue<DJI::OSDK::Telemetry::TOPIC_VELOCITY>().data;

		telemetry.velocityX = velocity.x;
		telemetry.velocityY = velocity.y;
		telemetry.velocityZ = velocity.z;
	}
		break;

	case TopicName::TOPIC_QUATERNION:
	{
		auto q =
			v->subscribe->getValue<DJI::OSDK::Telemetry::TOPIC_QUATERNION>();

		telemetry.qw = q.q0;
		telemetry.qx = q.q1;
		telemetry.qy = q.q2;
		telemetry.qz = q.q3;
	}
	break;

	default:
	{

	}
		break;
	}
}